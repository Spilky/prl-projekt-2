//
// PRL projekt 2: Minimum Extraction sort
// Created by David Spilka (xspilk00) on 10.3.16.
//

#include <mpi.h>
#include <iostream>
#include <fstream>

using namespace std;

#define TAG 0
#define EMPTY_FLAG -1
#define STOP_FLAG -2

int main(int argc, char *argv[])
{
    int processors;
    int myId;
    int myNumber = EMPTY_FLAG;
    MPI_Status stat;

    //MPI INIT
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &processors);
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);

    /*
    //K měření času
    double start, end;
    MPI_Barrier(MPI_COMM_WORLD);
    start = MPI_Wtime();
    */

    int firstLeaf = (processors + 1) / 2 - 1;

    if (myId == 0) {
        int number;
        int invar = firstLeaf;
        fstream fin;

        fin.open(argv[1], ios::in);

        while (fin.good()){
            number = fin.get();
            if(fin.eof())
                break;
            if (invar != firstLeaf)
                cout << " ";
            cout << number;
            MPI_Send(&number, 1, MPI_INT, invar, TAG, MPI_COMM_WORLD);
            invar++;
        }

        number = EMPTY_FLAG;
        while (invar != processors) {
            MPI_Send(&number, 1, MPI_INT, invar, TAG, MPI_COMM_WORLD);
            invar++;
        }

        fin.close();
        cout << endl;
    }

    if (myId >= firstLeaf){
        MPI_Recv(&myNumber, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat);
    }

    bool run = true;
    bool runChildren = true;
    int leftChild = myId * 2 + 1;
    int rightChild = myId * 2 + 2;
    int parent = (myId - (myId % 2 == 0 ? 2 : 1)) / 2;
    int leftChildNumber;
    int rightChildNumber;

    if (processors == 1 && myId == 0) {
        if (myNumber != EMPTY_FLAG) {
            cout << myNumber << endl;
        }
        run = false;
    }

    while (run) {
        if (myId < firstLeaf) {
            if (myId == 0 && myNumber != EMPTY_FLAG) {
                cout << myNumber << endl;
                myNumber = EMPTY_FLAG;
            }

            if (myNumber == EMPTY_FLAG && runChildren) {
                MPI_Recv(&leftChildNumber, 1, MPI_INT, leftChild, TAG, MPI_COMM_WORLD, &stat);
                MPI_Recv(&rightChildNumber, 1, MPI_INT, rightChild, TAG, MPI_COMM_WORLD, &stat);

                if (leftChildNumber != EMPTY_FLAG || rightChildNumber != EMPTY_FLAG) {
                    if (leftChildNumber != EMPTY_FLAG && rightChildNumber != EMPTY_FLAG) {
                        if (leftChildNumber < rightChildNumber) {
                            myNumber = leftChildNumber;
                            leftChildNumber = EMPTY_FLAG;
                        } else {
                            myNumber = rightChildNumber;
                            rightChildNumber = EMPTY_FLAG;
                        }
                    } else if (leftChildNumber != EMPTY_FLAG) {
                        myNumber = leftChildNumber;
                        leftChildNumber = EMPTY_FLAG;
                    } else if (rightChildNumber != EMPTY_FLAG) {
                        myNumber = rightChildNumber;
                        rightChildNumber = EMPTY_FLAG;
                    }
                } else {
                    runChildren = false;
                    leftChildNumber = STOP_FLAG;
                    rightChildNumber = STOP_FLAG;
                    if (myId == 0) {
                        run = false;
                    }
                }

                MPI_Send(&leftChildNumber, 1, MPI_INT, leftChild, TAG, MPI_COMM_WORLD);
                MPI_Send(&rightChildNumber, 1, MPI_INT, rightChild, TAG, MPI_COMM_WORLD);
            }
        }

        if (myId != 0){
            MPI_Send(&myNumber, 1, MPI_INT, parent, TAG, MPI_COMM_WORLD);
            MPI_Recv(&myNumber, 1, MPI_INT, parent, TAG, MPI_COMM_WORLD, &stat);
            if (myNumber == STOP_FLAG) {
                run = false;
            }
        }
    }

    /*
    //K měření času
    MPI_Barrier(MPI_COMM_WORLD);
    end = MPI_Wtime();
    if (myId == 0) {
        cerr << end-start << endl;
    }
    */

    MPI_Finalize();

    return 0;
}
