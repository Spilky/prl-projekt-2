#!/bin/bash
#
# PRL projekt 2: Minimum Extraction sort
# Created by David Spilka (xspilk00) on 10.3.16.
#

for j in {1..100}
do
    for i in 1 2 4 8 16 32
    do
        num_count=$i
        proc_count=$(($i*2-1))
        echo "------------------- TEST $i ($num_count - $proc_count) ------------------- "
        dd if=/dev/urandom bs=1 count=$num_count of=numbers # vytvorenie vstupneho suboru
        mpic++ --prefix /usr/local/share/OpenMPI -o mes mes.cpp # preklad zdrojaku
        mpirun --prefix /usr/local/share/OpenMPI -np $proc_count mes numbers $proc_count 2>> "./mereni/local/$i.csv" # spusetnie projektu
        rm mes numbers
    done
done